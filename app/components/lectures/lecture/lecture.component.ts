import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {ILecture} from "./lecture.model";

@Component({
  selector: 'lecture',
  //templateUrl: 'app.html',
  template: `
    Lecture #{{lecture.number}}: {{lecture.title}}
    <button type="button" (click)="onSelect()">
      View Details
    </button>
  `
})
export class LectureComponent implements OnInit {
  @Input() lecture: ILecture;
  @Output() select = new EventEmitter();

  constructor(){
    console.log(this.lecture);
  }

  ngOnInit(){
    console.log(this.lecture);
  }

  onSelect(): void {
    this.select.emit();
  }
}
