import { Routes } from '@angular/router';
import {LecturesComponent} from "./lectures";
import {CreateLectureComponent} from "./create-lecture/create-lecture";


export const lecturesRoutes: Routes = [
  {
    path: 'lectures',
    component: LecturesComponent
  }
];
