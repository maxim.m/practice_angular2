import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { LectureDetailsComponent }  from './lecture-details';
import LectureDetailsService from "./lecture-details.service";

@NgModule({
  imports: [
    BrowserModule
  ],
  declarations: [
    LectureDetailsComponent
  ],
  providers: [
    LectureDetailsService
  ],
  exports: [
    LectureDetailsComponent
  ]
})

export class LectureDetailsModule { }
