interface ILectureDetails {
  id: number;
  title: string;
  number: number;
  description: string;
  time: string;
  mentors?: string[];
  author: string;
}

class LectureDetails implements ILectureDetails {
  id: number;
  title: string;
  number: number;
  description: string;
  time: string;
  author: string;
}

export { ILectureDetails, LectureDetails };
