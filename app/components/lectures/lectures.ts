import {Component} from '@angular/core';
import {ILecture, LectureModel} from "./lecture/lecture.model";

@Component({
  moduleId: module.id,
  selector: 'lectures',
  templateUrl: 'lectures.html'
})

export class LecturesComponent {
  lectures: ILecture[] = [
    {
      id: 0,
      number: 1,
      title: 'HTML part 1'
    },
    {
      id: 1,
      number: 2,
      title: 'HTML part 2'
    },
    {
      id: 2,
      number: 3,
      title: 'CSS'
    }
  ];

  selectedLectureId: number = null;

  isCreateMode: boolean = false;
  newLecture: ILecture = new LectureModel();

  constructor(){}

  selectLecture(lecture: ILecture): void {
    this.selectedLectureId = lecture.id;
  }

  toggleCreate(isCreate: boolean): void {
    this.isCreateMode = isCreate;
  }

  addLecture(): void {
    //console.log(lecture);
    this.lectures.push(this.newLecture);
    this.newLecture = new LectureModel();
  }
}
