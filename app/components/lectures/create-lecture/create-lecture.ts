import {Component, EventEmitter, Output, Input} from '@angular/core';
import {ILecture, LectureModel} from "./../lecture/lecture.model";

@Component({
  moduleId: module.id,
  selector: 'lecture-create',
  templateUrl: './create-lecture.html'
})

export class CreateLectureComponent {
  @Output()
  create: EventEmitter<boolean> = new EventEmitter();
  @Output()
  cancel: EventEmitter<boolean> = new EventEmitter();

  @Input()
  lecture: ILecture;

  constructor(){}

  createLecture(): void {
    this.create.emit(true);
  }

  onCancel(): void {
    this.cancel.emit(true);
  }
}
