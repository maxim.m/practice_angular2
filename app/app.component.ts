import { Component } from '@angular/core';

@Component({
  moduleId: module.id, // fully resolved filename; defined at module load time
  selector: 'my-app',
  templateUrl: 'app.html'
})
export class AppComponent  {
  name: string = 'SPD-U';
  course: string = 'Front-End';
}
